﻿<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>AGENDA - Cadastro</title>
		<link href="../../css/style.css" rel="stylesheet"/>
		
		<script language="javascript" src="../../funcoes/funcoesjs.js"></script>

		<script>
			function validaClassificacao() { 
				if (document.form.dsClassificacao.value == "") { 
					alert ("Campo Classificacao nao Preenchido.");
					form.dsClassificacao.focus();
					return false
				}
			}
		</script>
	</head>
		
	<body>	
		<center>
			<div id="sobrecentro">• Cadastre a Classifica&ccedil;&atilde;o do Compromisso</div>
			<form action="verificaClassificacao.php" method="post"name="form" onSubmit="return validaClassificacao();"> 
				<table>
					<tr>
						<td><b><font color="red">*</font> Classifica&ccedil;&atilde;o:</b></td> 
						<td><input type="text" name="Classificacao" id="dsClassificacao"></td>
						<td> <span style="padding:100px"></td>
					</tr>						
					<tr>
						<td><input type="submit" value="Cadastar" class="button2" name="submit"></td>
						<td><input type="reset" value="limpar" class="button2" name="reset"></td>
					</tr>
					<tr>
						<td colspan="2"><br<font size="1"><b>Os campos com <font color="red">*</font> são de preenchimento obrigatótio</b></font></td>
					</tr>
					<tr>
						<td colspan="2"><a href="listagemClassificacao.php">Voltar</a></b></td>
					</tr> 
				</table>
			</form>
		</center>
	</body>
</html>
