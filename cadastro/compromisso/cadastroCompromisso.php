<html>
	<head>
		<title>AGENDA - Cadastro</title>

		<link rel="stylesheet" type="text/css" href="../../css/style.css"  />
		<script language="javascript" src="../../funcoes/funcoesjs.js"></script>

		<script>
			function validaCompromisso() { 
				if (document.form.dsCompromisso.value == "") { 
					alert ("Campo Compromisso nao Preenchido.");
					form.dsCompromisso.focus();
					return false
				}
				if (document.form.dtInicial.value == "") { 
					alert ("Campo Data Inicial nao Preenchido.");
					form.dtInicial.focus();
					return false
				}
				if (document.form.hrInicial.value == ""){
					alert ("Campo Hora Inicial nao Preenchido.");
					form.hrInicial.focus();
					return false
				}
				if (document.form.dtFinal.value == ""){
					alert ("Campo Data Final nao Preenchido.");
					form.dtFinal.focus();
					return false
				}
				if (document.form.hrInicial.value == ""){
					alert ("Campo Hora Final nao Preenchido.");
					form.hrInicial.focus();
					return false
				}
				if (document.form.dtInicial.value > document.form.dtFinal.value) { 
					alert ("Data Inicial n�o pode ser maior que a Data Final.");
					form.dtInicial.focus();
					return false
				}
				if ( (document.form.dtInicial.value = document.form.dtFinal.value) && (document.form.hrInicial.value > document.form.hrFinal.value)) { 
					alert ("Hora Inicial n�o pode ser maior que a Hora Final.");
					form.dtInicial.focus();
					return false
				}
			}
		</script>
	</head>
		
	<body>	
		<center>
			<div id="sobrecentro">� Cadastre o Compromisso</div>
				
			<form action="VerificaCompromisso.php" method="post"name="form" onSubmit="return validaCompromisso();"> 
				<table>
					<tr>
						<td><b>Compromisso <font color="red">*</font></b></td>
						<td><textarea name="Compromisso" id="dsCompromisso" maxlength="255" onblur ="validaCampoVazio(this);"></textarea>
						</td>
					</tr>
					<tr>
						<td><b>Data Incial <font color="red">*</font> </b></td>
						<td><input type="date" name="Data Inicial" id="dtInicial" maxlength="10" onblur ="validaCampoVazio(this);" OnKeyUp="mascaraData(this);"></td>
					</tr>
					<tr>
						<td><b>Hora Inicial <font color="red">*</font> </b></td>
						<td><input type="time" name="Hora Inicial" id="hrInicial" maxlength="10" onblur ="validaCampoVazio(this);" ></td>
					</tr>
					<tr>
						<td><b>Data Final <font color="red">*</font> </b></td>
						<td><input 	type="date" name="Data Final"   id="dtFinal" maxlength="10"  onblur ="validaCampoVazio(this);" OnKeyUp="mascaraData(this);"></td>
					</tr>
					<tr>
						<td><b>Hora Final <font color="red">*</font> </b></td>
						<td><input type="time" name="Hora Final" id="hrFinal" maxlength="10"  onblur ="validaCampoVazio(this);" ></td>
					</tr>
					<tr>
						<td><b>Local </b></td>
						<td><input type="text" name="dsLocal" id="dsLocal" maxlength="50"></td>
					</tr>
					<tr>
						<td><b>Classifica��o </b></td>
						<td>
							<select name="classificacao">
								<option value=""></option>
								<option value="1">Faculdade</option>
							</select>	
						</td>
					</tr>
					<tr>
						<td><b>Situa��o </b></td>
						<td>
							<select name="situacao">
								<option value=""></option>
								<option value="true">Completo</option>
								<option value="false">Pendente</option>							
							</select>	
						</td>
					</tr>
					<tr>
						<td><input type="submit" value="Cadastar" class="button2" name="submit"></td>
						<td><input type="reset" value="limpar" class="button2" name="reset"></td>
					</tr>
					<tr><td colspan="2">&nbsp;</td></tr> 
					<tr>
						<td colspan="2"><font size="1"><b>Os campos com <font color="red">*</font> s�o de preenchimento obrigat�tio</b></font></td>
					</tr>
					<tr><td colspan="2">&nbsp;</td></tr> 
					<tr><td colspan="2"><a href="listagemCompromisso.php">Voltar</a></b></td></tr> 
				</table>
			</form>
		</center>
	</body>
</html>
