<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>Agenda - Cadastro</title>
		<link href="../../css/style.css" rel="stylesheet"/>
		<script language="javascript" src="../../funcoes/funcoesjs.js"></script>
		<script>
			function validaFeriado() { 
				if (document.form.dsFeridado.value == "") { 
					alert ("Campo Feriado nao Preenchido.");
					form.dsFeridado.focus();
					return false
				}

				if (document.form.dtDia.value == "") { 
					alert ("Campo Data do Feriado nao Preenchido.");
					form.dtDia.focus();
					return false
				}
alert(document.form.tipo.value);
				if (document.form.tipo.value == "") { 
					alert ("Campo Tipo do Feriado nao Preenchido.");
					form.tipo.focus();
					return false
				}

				if (document.form.tipo.value == "E") { 
					
					if (document.form.dsEstado.value == "") { 
						alert ("Feriado Estadual. Escolha um Estado.");
						form.dsEstado.focus();
						return false
					}	
				}	

				if (document.form.tipo.value == "M") { 
					
					if (document.form.dsCidade.value == "") { 
						alert ("Feriado Municipal. Escolha uma Cidade.");
						form.dsEstado.focus();
						return false
					}	
				}							
			}
		</script>

	</head>

	<body>
		<center>
			<div id="sobrecentro">Cadastre o Feriado</div>
			<form action="verificaFeriado.php" method="post" onSubmit="return valida_feriado();" name="form">
				<table>
				<tr>
					<td><b><font color="red">*</font> Feriado:</b></td> 
					<td><input type="text" name="Feriado" id="dsFeridado"></td>
				</tr>
				<tr>
					<td><b><font color="red">*</font> Data:</b></td> 
					<td><input type="date" name="Data" id="dtDia" maxlength="10" onblur ="validaCampoVazio(this);" OnKeyUp="mascaraData(this);"></td>
				</tr>
				<tr>
					<td><b><font color="red">*</font> Tipo:</b></td> 
					<td><input type="radio" name="tipo" value="N">Nacional<br>
						<input type="radio" name="tipo" value="E">Estadual<br>
						<input type="radio" name="tipo" value="M">Municipal<br>
					</td>
				</tr>
				<tr>
					<td><b>Estado:</b></td> 
					<td>
						<select id="dsEstado">
							<option value=""></option>
							<option value="AC">Acre</option>
							<option value="AL">Alagoas</option>
							<option value="AP">Amap&aacute;</option>
							<option value="AM">Amazonas</option>
							<option value="BA">Bahia</option>
							<option value="CE">Cear&aacute;</option>
							<option value="DF">Distrito Federal</option>
							<option value="ES">Esp&iacute;rito Santo</option>
							<option value="GO">Goi&aacute;s</option>
							<option value="MA">Maranh&atilde;o</option>
							<option value="MT">Mato Grosso</option>
							<option value="MS">Mato Grosso do Sul</option>
							<option value="MG">Minas Gerais</option>
							<option value="PA">Par&aacute;</option>
							<option value="PB">Para&iacute;ba</option>
							<option value="PR">Paran&aacute;</option>
							<option value="PE">Pernambuco</option>
							<option value="PI">Piau&iacute;</option>
							<option value="RJ">Rio de Janeiro</option>
							<option value="RN">Rio Grande do Norte</option>
							<option value="RS">Rio Grande do Sul</option>
							<option value="RO">Rond&ocirc;nia</option>
							<option value="RR">Roraima</option>
							<option value="SC">Santa Catarina</option>
							<option value="SP">S&atilde;o Paulo</option>
							<option value="SE">Sergipe</option>
							<option value="TO">Tocantins</option>								
						</select>	
					</td>
				</tr>
				<tr>
					<td><b>Cidade:</b></td> 
					<td>
						<select id="dsCidade">
						    <option value=""></option>
							<option value="01">Balne&aacute;rio Cambori&uacute;</option>
							<option value="02">Blumenau</option>
							<option value="03">Florianopolis</option>
							<option value="04">Joinville</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><input type="submit" value="enviar" class="button2" name="submit"></td>
					<td><input type="reset" value="limpar" class="button2" name="reset"></td>
				</tr>
				<tr>
					<td colspan="2"><br><b><font size="2">Os campos com <font color="red">*</font> s&atilde;o de preenchimento obrigat&oacute;tio</font></b></td>
				</tr>
				<tr>
					<td colspan="2"><a href="listagemFeriado.php">Voltar</a></b></td>
				</tr> 
				</table>
			</form>
		</center>
	</body>

</html>

