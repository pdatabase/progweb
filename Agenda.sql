CREATE TABLE Usuario (
  idUsuario VARCHAR(255)   NOT NULL ,
  cd_senha VARCHAR(255)    ,
  nm_usuario VARCHAR(255)    ,
  ds_email VARCHAR(500)    ,
  nr_telefone NUMERIC(9)      ,
PRIMARY KEY(idUsuario));




CREATE TABLE Classificacao_Compromisso (
  idClassificacao_Compromisso INTEGER   NOT NULL ,
  ds_Classificacao VARCHAR(255)      ,
PRIMARY KEY(idClassificacao_Compromisso));




CREATE TABLE Cidade (
  idCidade INTEGER   NOT NULL ,
  ds_Cidade VARCHAR(255)    ,
  ds_UF VARCHAR(2)      ,
PRIMARY KEY(idCidade));




CREATE TABLE Feriados (
  idFeriados INTEGER   NOT NULL ,
  Cidade_idCidade INTEGER    ,
  ds_feriado VARCHAR(255)    ,
  dt_dia DATE    ,
  id_tipo VARCHAR(1)  DEFAULT 'M','E','F'  ,
  ds_uf VARCHAR(2)      ,
PRIMARY KEY(idFeriados)  ,
  FOREIGN KEY(Cidade_idCidade)
    REFERENCES Cidade(idCidade));


CREATE INDEX Feriados_FKIndex1 ON Feriados (Cidade_idCidade);

COMMENT ON COLUMN Feriados.id_tipo IS 'Municipal, Estadual  e Federal';

CREATE INDEX IFK_Rel_06 ON Feriados (Cidade_idCidade);


CREATE TABLE Compromisso (
  idCompromisso INTEGER   NOT NULL ,
  idUsuario VARCHAR(255)   NOT NULL ,
  idClassificacao_Compromisso INTEGER   NOT NULL ,
  dt_Inicio DATE   NOT NULL ,
  hr_Inicio TIME    ,
  dt_Fim DATETIME   NOT NULL ,
  ht_fim TIME    ,
  ds_Compromisso VARCHAR(255)    ,
  ds_Local VARCHAR(255)    ,
  id_completo BOOL      ,
PRIMARY KEY(idCompromisso)    ,
  FOREIGN KEY(idClassificacao_Compromisso)
    REFERENCES Classificacao_Compromisso(idClassificacao_Compromisso),
  FOREIGN KEY(idUsuario)
    REFERENCES Usuario(idUsuario));


CREATE INDEX Compromisso_FKIndex1 ON Compromisso (idClassificacao_Compromisso);
CREATE INDEX Compromisso_FKIndex2 ON Compromisso (idUsuario);


CREATE INDEX IFK_Rel_01 ON Compromisso (idClassificacao_Compromisso);
CREATE INDEX IFK_Rel_02 ON Compromisso (idUsuario);



